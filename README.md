# `Prelude.res`

A set of basic functions extending the `Js` and/or `Belt` modules.

## `Prelude.PromiseExported`

Includes all the functions of `Js.Promise2` (since 5.0) and adds:

```
type t<+'a> = promise<'a>
```
Note: Since promises are everywhere in browser code, most of the functions in this module are always included in `Prelude`.  So `open Prelude` is enough to access the most common functions.  This might change in a future (or final release of 5.0.0) because of the new `async/await` syntax in ReScript 10.1.


### `thenResolve: (promise<'a>, 'a => 'b) => promise<'b>`

Takes a `fn` function  and maps the resolved value.

Since ReScript 10.1 we expect this function to become less and less useful  because

```rescript
promise->thenResolve(fn)
```

is the same as

```rescript
fn(await promise)
```

### `catchResolve: (promise<'a>, error => 'a): promise<'a>`

Similar to `thenResolve` but using `catch`.  This is useful to turn errors into values.


### `rejectWithError: exn => promise<result<'a, error>>`

takes an exception an returns a promise that rejects with `Error(Js.Promise2.error)`

### `finally: (promise<'a>, 'a => unit) => promise<'a>`

Binding to [JavaScript's `finally`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/finally).

### `thenDo: (promise<'a>, 'a => unit) => unit`

Executes a function when the promise resolves; return the unit value `()`.

## `Prelude.Promises`

Includes all the functions from module `PromiseExported` but those are not exported.  Notice the name is `Promises` (plural form), to avoid clashes with the browser's native `Promise`.

### `ellapsed: int => promise<unit>`

Return a promise that resolve in the given time (in milliseconds).  This just wraps `Js.Global.setTimeout` as a promise.

Note: This function is not included in `Prelude`.

### `result: promise<'a> => promise<result<'a, error>>`

Converts a promise to another that always resolves to a `result`; it never rejects, but instead resolves to an `Error`.

Note: These functions are not included in `Prelude`.

### `iter: array<unit => 'a> => promise<array<'a>>`

This similar to `Promises.all`, but we execute the functions iteratively.


## `Prelude.PromisedResult`

This module provides functions to the type

```
type t<'a, 'e> = promise<result<'a, 'e>>
```

In the following will use the alias `t<'a, 'e>` instead of the longer type `promise<result<'a, 'e>>`.

### `map: (t<'a, 'e>, 'a => 'b) => t<'b, 'e>`

### `mapWithDefault: (t<'a, 'e>, 'b, 'a => 'b) => promise<'b>`

### `flatMap: (t<'a, 'e>, 'a => result<'b, 'e>) => t<'b, 'e>`

### `mapError: (t<'a, 'e>, 'e => 'c) => t<'a, 'c>`

### `flatMapError: (t<'a, 'e>, 'e => result<'a, 'c>) => t<'a, 'c>`

### `bind: (t<'a, 'e>, 'a => t<'b, 'e>) => t<'b, 'e>`

### `flip: t<'a, 'e> => t<'e, 'a>`

Flip Ok to Error, and Error to Ok.

### `ok: t<'a, 'e> => promise<option<'a>>`

Convert a promise resolving to result, to one that resolves to an `option` discarding Errors.

### `warn: t<'a, 'e> => promise<option<'a>>`

Convert a promise resolving to result, to one that resolves to an `option` discarding Errors, but issuing a console warning.

### `scream: t<'a, 'e> => promise<option<'a>>`

Convert a promise resolving to result, to one that resolves to an `option` discarding Errors, but issuing a console error.


### `bail: array<unit => t<'a, 'e>> => t<array<'a>, 'e>`

Run each function in turn until one resolves to an Error, or all resolve to Ok.  Return `Ok(results)` with the collected results or the first `Error`.

### `chain: ('a, array<'a => t<'a, 'e>>) => t<'a, 'e>`

Run each function in turn passing each Ok result to the next function.  Stop as soon as a function returns Error.


### `untilOk: array<unit => t<'a, 'e>> => t<option<'a>, 'e>`

Execute the actions one by one and resolve to the first Ok result, if no action returns Ok, resolve to the last Error result.

The only case where this resolves to None is when `actions` is the empty  array.
